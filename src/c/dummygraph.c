/* XLISP-STAT 2.1 Copyright (c) 1990, by Luke Tierney                  */
/* Additions to Xlisp 2.1, Copyright (c) 1989 by David Michael Betz    */
/* You may give out copies of this software; for conditions see the    */
/* file COPYING included with this distribution.                       */
extern void xlfail(char*);
void StInitGraphics() {}

int StScreenHasColor() { return(0); }
void StGetScreenSize() {}
void StFlushGraphics(){}
void StWSetTitle() {}
void StHideWindow() {}
void StShowWindow() {}
int StHasWindows() { return(0); }

void StWSetLocation() { xlfail("not supported"); }
void StWGetLocation() { xlfail("not supported"); }
void StWGetSize() { xlfail("not supported"); }
void StWSetSize() { xlfail("not supported"); }
void StGWSetSize() { xlfail("not supported"); }
void StGWShowWindow() { xlfail("not supported"); }
void IViewWindowGetObject() { xlfail("not supported"); }
void IViewWindowWinInfo() { xlfail("not supported"); }
int StGWWinInfoSize() { return(1); }
void StGWInitWinInfo() {}
void IViewGetNewBrushSize() { xlfail("not supported"); }
void StGWInitialDraw() { xlfail("not supported"); }
void StGWFrameRect() { xlfail("not supported"); }
void StGWDrawMode() { xlfail("not supported"); }
void StGWSetDrawMode() { xlfail("not supported"); }
void StGWLineType() { xlfail("not supported"); }
void StGWSetLineType() { xlfail("not supported"); }
void StGWWhileButtonDown() { xlfail("not supported"); }

void IViewWindowNew() { xlfail("not supported"); }
void StGWSetIdleOn() { xlfail("not supported"); }
void StGWSetObject() {}
void StGWSetDrawColor() { xlfail("not supported"); }
void StGWSetBackColor() { xlfail("not supported"); }
void StGWGetScreenSize() { xlfail("not supported"); }
void StGWSetHasHscroll() { xlfail("not supported"); }
void StGWSetHasVscroll() { xlfail("not supported"); }
void StGWIdleOn() { xlfail("not supported"); }
void StGWRemove() { xlfail("not supported"); }
void StGWSetTitle() { xlfail("not supported"); }
void StGWSetUseColor() { xlfail("not supported"); }
void StGWCanvasWidth() { xlfail("not supported"); }
void StGWCanvasHeight() { xlfail("not supported"); }
void StGWDrawColor() { xlfail("not supported"); }
void StGWBackColor() { xlfail("not supported"); }
void StGWUseColor() { xlfail("not supported"); }
void StGWReverseColors() { xlfail("not supported"); }
void StGWGetViewRect() { xlfail("not supported"); }
void StGWSetLineWidth() { xlfail("not supported"); }
void StGWGetLineWidth() { xlfail("not supported"); }
void StGWHasHscroll() { xlfail("not supported"); }
void StGWHasVscroll() { xlfail("not supported"); }
void StGWSetScroll() { xlfail("not supported"); }
void StGWGetScroll() { xlfail("not supported"); }
void StGWSetHscrollIncs() { xlfail("not supported"); }
void StGWSetVscrollIncs() { xlfail("not supported"); }
void StGWGetHscrollIncs() { xlfail("not supported"); }
void StGWGetVscrollIncs() { xlfail("not supported"); }
void StGWDrawLine() { xlfail("not supported"); }
void StGWEraseRect() { xlfail("not supported"); }
void StGWPaintRect() { xlfail("not supported"); }
void StGWEraseOval() { xlfail("not supported"); }
void StGWFrameOval() { xlfail("not supported"); }
void StGWPaintOval() { xlfail("not supported"); }
void StGWTextAscent() { xlfail("not supported"); }
void StGWTextWidth() { xlfail("not supported"); }
void StGWDrawStringUp() { xlfail("not supported"); }
void StGWDrawString() { xlfail("not supported"); }
void StGWDrawTextUp() { xlfail("not supported"); }
void StGWDrawText() { xlfail("not supported"); }
void StGWDrawSymbol() { xlfail("not supported"); }
void StGWReplaceSymbol() { xlfail("not supported"); }
void StGWStartBuffering() { xlfail("not supported"); }
void StGWBufferToScreen() { xlfail("not supported"); }
void StGWSetClipRect() { xlfail("not supported"); }
void StGWGetClipRect() { xlfail("not supported"); }
void StGWSetCursor() { xlfail("not supported"); }
void StGWCursor() { xlfail("not supported"); }
void StGWResetBuffer() { xlfail("not supported"); }
void StGWSetColRefCon() {}
void StGWGetColRefCon() { xlfail("not supported"); }
void StGWSetSymRefCon() {}
void StGWGetSymRefCon() { xlfail("not supported"); }
void StGWSetCursRefCon() {}
void StGWGetCursRefCon() { xlfail("not supported"); }
void StGWFreeCursor() { xlfail("not supported"); }
void StGWMakeCursor() { xlfail("not supported"); }
void StGWMakeResCursor() { xlfail("not supported"); }
void StGWErasePoly() { xlfail("not supported"); }
void StGWPaintArc() { xlfail("not supported"); }
void StGWEraseArc() { xlfail("not supported"); }
void StGWFrameArc() { xlfail("not supported"); }
void StGWFreeColor() { xlfail("not supported"); }
void StGWMakeColor() { xlfail("not supported"); }
void StGWTextDescent() { xlfail("not supported"); }
void StGWFramePoly() { xlfail("not supported"); }
void StGWPaintPoly() { xlfail("not supported"); }
void StGWGetRefCon() { xlfail("not supported"); }
void StGWSetRefCon() { xlfail("not supported"); }
void StGWSetFreeMem() { xlfail("not supported"); }
void StGWIsActive() { xlfail("not supported"); }
void StGWGetSymbolSize() { xlfail("not supported"); }
void StGWDumpImage() { xlfail("not supported"); }
void StGWDrawPoint() { xlfail("not supported"); }
void StGWDrawBitmap() { xlfail("not supported"); }

void DialogRemove() { xlfail("not supported"); }
void DialogAllocate() { xlfail("not supported"); }
void DialogSetDefaultButton() { xlfail("not supported"); }
void DialogGetModalItem() { xlfail("not supported"); }
void DialogButtonGetDefaultSize() { xlfail("not supported"); }
void DialogToggleGetDefaultSize() { xlfail("not supported"); }
void DialogToggleItemValue() { xlfail("not supported"); }
void DialogTextGetDefaultSize() { xlfail("not supported"); }
void DialogTextItemText() { xlfail("not supported"); }
void DialogChoiceGetDefaultSize() { xlfail("not supported"); }
void DialogChoiceItemValue() { xlfail("not supported"); }
void DialogScrollGetDefaultSize() { xlfail("not supported"); }
void DialogScrollItemValue() { xlfail("not supported"); }
void DialogScrollItemMax() { xlfail("not supported"); }
void DialogScrollItemMin() { xlfail("not supported"); }
void DialogListGetDefaultSize() { xlfail("not supported"); }
void DialogListItemSetText() { xlfail("not supported"); }
void DialogListItemSelection() { xlfail("not supported"); }

void StMObDisposeMach () {}
void StMObDeleteItem () {}
void StMObSetItemProp () {}
void StMObAppendItems () {}
void StMObAllocateMach () {}
void StMObRemove () {}
int StMObInstalled () { return(0); }
void StMObEnable () {}
void StMObInstall () {}
void StMObPopup () {}

